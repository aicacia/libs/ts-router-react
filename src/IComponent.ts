export type INoPropsComponent = () => React.ReactNode;
export type IFunctionComponent<P> = (props: P) => React.ReactNode;

export type IComponent<P = any, S = any> =
  | INoPropsComponent
  | IFunctionComponent<P>
  | React.FC<P>
  | React.ComponentType<P>
  | React.ComponentClass<P, S>;

import { isArray } from "util";

export function toArray<T>(value: any): Array<T> {
  if (isArray(value)) {
    return value;
  } else if (value) {
    return [value];
  } else {
    return [];
  }
}

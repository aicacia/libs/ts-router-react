import { ILayerProps, Layer } from "./Layer";

export type IComponentElement = React.ComponentElement<ILayerProps, Layer>;

// @ts-ignore
import { JSDOM } from "jsdom";

import * as Enzyme from "enzyme";
import * as EnzymeAdapter from "enzyme-adapter-react-16";
import { parse } from "url";
import * as React from "react";
import * as tape from "tape";
import { Router, Route, Scope, Middleware } from ".";
import { NodeAsyncLocation } from "@aicacia/location";
import {
  Router as AicaciaRouter,
  IContext,
  UnhandledError,
  createContext,
} from "@aicacia/router";
import { IComponentProps } from "./Route";

const dom = new JSDOM("<!doctype html><html><body></body></html>");

(global as any).document = dom.window.document;
(global as any).window = dom.window;

Enzyme.configure({ adapter: new EnzymeAdapter() });

const wait = (ms: number): Promise<void> =>
  new Promise((resolve) => setTimeout(resolve, ms));

const router = new AicaciaRouter();
const location = new NodeAsyncLocation(parse("http://test.com/", true));

const onRejected = (error: Error, context: IContext) => {
  if (error.message === UnhandledError.MESSAGE) {
    return Promise.reject(parse("/404", true));
  } else {
    context.error = error;
    return Promise.reject(parse("/500", true));
  }
};

const Loading = () => "Loading...";
const NotFound = () => "404 - Not Found";
const InternalAppError = () => "500 - Internal Application Error";

let onChange_called = 0;
const onChange = () => onChange_called++;

const middlewareError = () => Promise.reject(new Error("FAIL"));
const middlewareAddData = (context: IContext) => {
  context.data = "Hello, world!";
};

const Component = (props: IComponentProps) => props.context.data;

interface IRootState {
  context: IContext;
  newPath?: string;
}

class Root extends React.PureComponent<Record<string, unknown>, IRootState> {
  state: IRootState = {
    context: createContext(location.get()),
  };

  addNewPath(newPath: string) {
    this.setState({ newPath });
  }
  onFulfilled = (context: IContext) => {
    this.setState({ context });
    return context.url;
  };

  render() {
    return (
      <Router
        location={location}
        router={router}
        onFulfilled={this.onFulfilled}
        onRejected={onRejected}
        Loading={Loading}
        onChange={onChange}
        context={this.state.context}
      >
        <Route path="/404" Component={NotFound} />
        <Route path="/500" Component={InternalAppError} />
        <Middleware path="/error" handler={middlewareError} />
        <Scope path="/scope">
          <Middleware handler={middlewareAddData} />
          <Route path="/component" Component={Component} />
        </Scope>
        {this.state.newPath && (
          <Route path={this.state.newPath} Component={Component} />
        )}
      </Router>
    );
  }
}

tape("connect update", async (assert: tape.Test) => {
  const wrapper = Enzyme.mount(<Root />);

  assert.equals(wrapper.getDOMNode().textContent, Loading());

  // wait for location.init
  await wait(1);
  assert.equals(wrapper.getDOMNode().textContent, NotFound());

  await location.set("/error");
  assert.equals(wrapper.getDOMNode().textContent, InternalAppError());

  await location.set("/scope/component");
  assert.equals(wrapper.getDOMNode().textContent, "Hello, world!");
  assert.equals(onChange_called, 5);

  (wrapper.instance() as Root).addNewPath("/new-path");

  assert.end();
});

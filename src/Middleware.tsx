import { Layer, ILayerProps } from "./Layer";
import { IHandler } from "@aicacia/router";

export interface IMiddlewareProps extends ILayerProps {
  handler: IHandler;
}

export class Middleware extends Layer<IMiddlewareProps> {
  render() {
    return null;
  }
}

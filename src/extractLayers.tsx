import { Router, IContext } from "@aicacia/router";
import { Scope, IScopeProps } from "./Scope";
import { Middleware, IMiddlewareProps } from "./Middleware";
import { IRouteProps } from "./Route";
import { toArray } from "./toArray";
import { IComponentElement } from "./IComponentElement";

export function extractLayers(
  router: Router,
  routeElements: Array<IComponentElement>
) {
  routeElements
    .filter((routeElement) => !!routeElement)
    .forEach((routeElement) => {
      if ((routeElement.type as any) === Scope) {
        const props = routeElement.props as IScopeProps;
        extractLayers(
          router.scope(props.path, props.meta || {}),
          toArray(props.children)
        );
      } else if ((routeElement.type as any) === Middleware) {
        const props = routeElement.props as IMiddlewareProps;
        router.use(props.path || "/", props.meta || {}, props.handler);
      } else {
        const props = routeElement.props as IRouteProps,
          Component = props.Component;

        router.route(
          props.path || "/",
          props.meta || {},
          (context: IContext) => {
            context.Component = Component;
            context.end();
          }
        );
      }
    });
}

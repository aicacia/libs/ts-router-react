import { Layer, ILayerProps } from "./Layer";
import { IRouterChildren } from "./Router";

export interface IScopeProps extends ILayerProps {
  children: IRouterChildren;
}

export class Scope extends Layer<IScopeProps> {
  render() {
    return null;
  }
}

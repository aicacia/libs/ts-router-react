import * as React from "react";
import { UrlWithParsedQuery } from "url";
import {
  Router as AicaciaRouter,
  createContext,
  IContext,
} from "@aicacia/router";
import { AsyncLocation } from "@aicacia/location";
import { extractLayers } from "./extractLayers";
import { shouldUpdate } from "./shouldUpdate";
import { toArray } from "./toArray";
import { IComponent } from "./IComponent";
import { IComponentElement } from "./IComponentElement";

export interface ILoadingProps {
  context: IContext;
}

export type IRouterChildren =
  | IComponentElement
  | null
  | undefined
  | ""
  | Array<IComponentElement | null | undefined | "">;

export const DefaultLoading = (props: ILoadingProps) =>
  `Loading ${props.context.url.href}`;

export interface IRouterProps {
  location: AsyncLocation;
  router: AicaciaRouter;
  onChange?: (context: IContext) => void;
  onFulfilled?: (
    context: IContext
  ) => Promise<UrlWithParsedQuery> | UrlWithParsedQuery | null;
  onRejected?: (
    error: Error,
    context: IContext
  ) => Promise<UrlWithParsedQuery> | UrlWithParsedQuery | null;
  children: IRouterChildren;
  Loading?: IComponent<ILoadingProps>;
  context: IContext;
}

export class Router extends React.Component<IRouterProps> {
  constructor(props: IRouterProps) {
    super(props);

    this.props.location.setHandler(() => this.handler);
  }

  handler = (url: UrlWithParsedQuery) => {
    const context = createContext(url);

    this.props.onChange && this.props.onChange(context);

    return this.props.router.handle(context).then(
      () => {
        if (context.redirectUrl) {
          return Promise.reject(context.redirectUrl);
        } else {
          let result = context.url;

          if (this.props.onFulfilled) {
            const customResult = this.props.onFulfilled(context);

            if (isUrlWithParsedQuery(customResult)) {
              result = customResult;
            } else if (isPromise(customResult)) {
              return customResult.then((url) => {
                return url;
              });
            }
          }

          return result;
        }
      },
      (error) => {
        if (this.props.onRejected) {
          return this.props.onRejected(error, context);
        } else {
          return Promise.reject(error);
        }
      }
    );
  };

  componentDidMount() {
    this.props.router.clear();
    extractLayers(this.props.router, toArray(this.props.children));
    this.props.location.init();
  }
  UNSAFE_componentWillReceiveProps(nextProps: IRouterProps) {
    if (this.props.location !== nextProps.location) {
      this.props.location.setHandler(() => this.handler);
    }
    if (
      shouldUpdate(toArray(this.props.children), toArray(nextProps.children))
    ) {
      this.props.router.clear();
      extractLayers(this.props.router, toArray(nextProps.children));
    }
  }
  render() {
    const Loading = (this.props.Loading ||
        DefaultLoading) as React.ComponentType<ILoadingProps>,
      Component = this.props.context.Component;

    return Component ? (
      <Component context={this.props.context} />
    ) : (
      <Loading context={this.props.context} />
    );
  }
}

function isUrlWithParsedQuery(value: any): value is UrlWithParsedQuery {
  return value && value.query && typeof value.query === "object";
}

function isPromise<T>(value: any): value is Promise<T> {
  return value && typeof value.then === "function";
}

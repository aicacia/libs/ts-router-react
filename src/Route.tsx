import { IContext } from "@aicacia/router";
import { Layer, ILayerProps } from "./Layer";
import { IComponent } from "./IComponent";

export interface IComponentProps {
  context: IContext;
}

export interface IRouteProps extends ILayerProps {
  Component: IComponent<IComponentProps>;
}

export class Route extends Layer<IRouteProps> {
  render() {
    return null;
  }
}

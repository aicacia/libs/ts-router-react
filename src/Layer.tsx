import * as React from "react";

export interface ILayerProps {
  path?: string;
  meta?: Record<string, any>;
}

export class Layer<P extends ILayerProps = ILayerProps> extends React.Component<
  P
> {
  static defaultProps: Partial<ILayerProps> = {
    path: "/",
  };

  render() {
    return null;
  }
}

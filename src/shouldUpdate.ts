import { Scope, IScopeProps } from "./Scope";
import { Middleware, IMiddlewareProps } from "./Middleware";
import { IRouteProps } from "./Route";
import { toArray } from "./toArray";
import { IComponentElement } from "./IComponentElement";

export function shouldUpdate(
  children: Array<IComponentElement>,
  nextChildren: Array<IComponentElement>
) {
  if (children.length !== nextChildren.length) {
    return true;
  } else {
    for (let i = 0, len = children.length; i < len; i++) {
      const child = children[i],
        nextChild = nextChildren[i];

      if (!child && !nextChild) {
        return false;
      }
      if (!!child !== !!nextChild) {
        return true;
      }

      if (child.type === Scope && nextChild.type === Scope) {
        const childProps = child.props as IScopeProps,
          nextChildProps = nextChild.props as IScopeProps;

        if (childProps.path !== nextChildProps.path) {
          return true;
        } else if (
          shouldUpdate(
            toArray(childProps.children),
            toArray(nextChildProps.children)
          )
        ) {
          return true;
        }
      } else if (
        (child.type as any) === Middleware &&
        (nextChild.type as any) === Middleware
      ) {
        const childProps = child.props as IMiddlewareProps,
          nextChildProps = nextChild.props as IMiddlewareProps;

        if (
          childProps.path !== nextChildProps.path ||
          childProps.handler !== nextChildProps.handler
        ) {
          return true;
        }
      } else {
        const childProps = child.props as IRouteProps,
          nextChildProps = nextChild.props as IRouteProps;

        if (
          childProps.path !== nextChildProps.path ||
          childProps.Component !== nextChildProps.Component
        ) {
          return true;
        }
      }
    }
    return true;
  }
}

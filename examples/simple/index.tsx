import { default as React } from "react";
import { render } from "react-dom";
import { parse } from "url";
import { BrowserAsyncLocation } from "@aicacia/location";
import {
  Router as AicaciaRouter,
  IContext,
  createContext,
} from "@aicacia/router";
import { Router, Route, Middleware, Scope, IComponentProps } from "../../src";

const router = new AicaciaRouter();
const location = new BrowserAsyncLocation(true);

const middleware = () => console.log("ALL Middleware");
const userMiddleware = () => console.log("User Middleware");

const Index = () => (
  <>
    <h1>Hello, Index!</h1>
    <a href="/user">User</a>
  </>
);
const User = (props: IComponentProps) => {
  console.log(props);
  return (
    <>
      <h1>Hello, User!</h1>
      <a href="/">Home</a>
    </>
  );
};

const NotFound = () => (
  <>
    <h1>404 - Not Found</h1>
    <a href="/">Home</a>
  </>
);

const InternalApplicationError = (props: IComponentProps) => (
  <>
    <h1>500 - Internal Application Error</h1>
    <p>{props.context.error}</p>
    <a href="/">Home</a>
  </>
);

interface IRootProps {}
interface IRootState {
  context: IContext;
}

class Root extends React.Component<IRootProps, IRootState> {
  constructor(props: IRootProps) {
    super(props);

    this.state = {
      context: createContext(location.get()),
    };
  }
  onRouteFulfilled = (context: IContext) => {
    if (!context.resolved()) {
      return Promise.reject(parse("/404"));
    }
  };
  onRouteRejected = (error, context: IContext) => {
    if (!context.resolved()) {
      return Promise.reject(parse("/404", true));
    } else if (error) {
      console.error(error);
      context.error = error;
      return Promise.reject(parse("/500", true));
    }
  };
  render() {
    return (
      <Router
        location={location}
        router={router}
        onFulfilled={this.onRouteFulfilled}
        onRejected={this.onRouteRejected}
        context={this.state.context}
      >
        <Route path="/404" Component={NotFound} />
        <Route path="/500" Component={InternalApplicationError} />
        <Middleware handler={middleware} />
        <Route Component={Index} />
        <Scope path="/user">
          <Middleware handler={userMiddleware} />
          <Route Component={User} />
        </Scope>
      </Router>
    );
  }
}

function onLoad() {
  render(<Root />, document.getElementById("root"));
}

window.addEventListener("load", onLoad);
